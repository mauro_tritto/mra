package tdd.training.mra;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

public class MarsRoverTest {

	@Test
	public void testInitializePlanetWithObstacles() throws MarsRoverException {
		int maxX = 10;
		int maxY = 10;
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("(4,7)");
		MarsRover rover = new MarsRover(maxX, maxY, planetObstacles);
		boolean obstacle = rover.planetContainsObstacleAt(4, 7);
		assertTrue(obstacle);
	}
	
	@Test
	public void testRoverShouldReturnItsLandingPosition() throws MarsRoverException {
		int maxX = 10;
		int maxY = 10;
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("(4,7)");
		planetObstacles.add("(2,3)");
		MarsRover rover = new MarsRover(maxX, maxY, planetObstacles);
		String commandString = "";
		String position = rover.executeCommand(commandString);
		assertEquals("(0,0,N)", position);
	}
	
	@Test
	public void testRoverShouldPointTowardsEast() throws MarsRoverException {
		int maxX = 10;
		int maxY = 10;
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("(4,7)");
		planetObstacles.add("(2,3)");
		MarsRover rover = new MarsRover(maxX, maxY, planetObstacles);
		String commandString = "r";
		String position = rover.executeCommand(commandString);
		assertEquals("(0,0,E)", position);
	}
	
	@Test
	public void testRoverShouldMoveForward() throws MarsRoverException {
		int maxX = 10;
		int maxY = 10;
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("(4,7)");
		planetObstacles.add("(2,3)");
		MarsRover rover = new MarsRover(maxX, maxY, planetObstacles, 7, 6, 'N');
		String commandString = "f";
		String position = rover.executeCommand(commandString);
		assertEquals("(7,7,N)", position);
	}
	
	@Test
	public void testRoverShouldMoveBackward() throws MarsRoverException {
		int maxX = 10;
		int maxY = 10;
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("(4,7)");
		planetObstacles.add("(2,3)");
		MarsRover rover = new MarsRover(maxX, maxY, planetObstacles, 5, 8, 'E');
		String commandString = "b";
		String position = rover.executeCommand(commandString);
		assertEquals("(4,8,E)", position);
	}
	
	@Test
	public void testSequenceOfSingleCommands() throws MarsRoverException {
		int maxX = 10;
		int maxY = 10;
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("(4,7)");
		planetObstacles.add("(2,3)");
		MarsRover rover = new MarsRover(maxX, maxY, planetObstacles);
		String commandString = "ffrff";
		String position = rover.executeCommand(commandString);
		assertEquals("(2,2,E)", position);
	}
	
	@Test
	public void testRoverShouldMoveBeyondAnEdge() throws MarsRoverException {
		int maxX = 10;
		int maxY = 10;
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("(4,7)");
		planetObstacles.add("(2,3)");
		MarsRover rover = new MarsRover(maxX, maxY, planetObstacles);
		String commandString = "b";
		String position = rover.executeCommand(commandString);
		assertEquals("(0,9,N)", position);
	}

}
