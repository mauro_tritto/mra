package tdd.training.mra;

public class Cell {
	public int x;
	public int y;
	public boolean isObstacle;

	public Cell(int x, int y, boolean isObstacle) {
		this.x = x;
		this.y = y;
		this.isObstacle = isObstacle;
	}

}
