package tdd.training.mra;

import java.util.List;

public class MarsRover {

	private Cell[][] planet;
	private int x;
	private int y;
	private char direction;

	/**
	 * It initializes the rover at the coordinates (0,0), facing North, on a planet
	 * (represented as a grid with x and y coordinates) containing obstacles.
	 * 
	 * @param planetX         The x dimension of the planet.
	 * @param planetY         The y dimension of the planet.
	 * @param planetObstacles The obstacles on the planet. Each obstacle is a string
	 *                        (without white spaces) formatted as follows:
	 *                        "(oi_x,oi_y)". <code>null</code> if the planet does
	 *                        not contain obstacles.
	 * 
	 * @throws MarsRoverException
	 */
	public MarsRover(int planetX, int planetY, List<String> planetObstacles) throws MarsRoverException {
		this.planet = new Cell[planetX][planetY];
		for (int i = 0; i < planetX; i++) {
			for (int j = 0; j < planetY; j++) {
				this.planet[i][j] = new Cell(i, j, false);
			}
		}
		for (int k = 0; k < planetObstacles.size(); k++) {
			String rawObstacle = planetObstacles.get(k);
			String obstacleCoord = rawObstacle.substring(1, rawObstacle.length() - 1);
			String[] coordArray = obstacleCoord.split(",");
			this.planet[Integer.parseInt(coordArray[0])][Integer.parseInt(coordArray[1])].isObstacle = true;
		}

		this.x = 0;
		this.y = 0;
		this.direction = 'N';
	}

	/**
	 * It initializes the rover at a given coordinates and direction on a planet
	 * (represented as a grid with x and y coordinates) containing obstacles.
	 * 
	 * @param planetX         The x dimension of the planet.
	 * @param planetY         The y dimension of the planet.
	 * @param planetObstacles The obstacles on the planet. Each obstacle is a string
	 *                        (without white spaces) formatted as follows:
	 *                        "(oi_x,oi_y)". <code>null</code> if the planet does
	 *                        not contain obstacles.
	 * @param x               The x coordinate of the rover.
	 * @param y               The y coordinate of the rover.
	 * @param direction       The direction of the rover
	 * 
	 * @throws MarsRoverException
	 */
	public MarsRover(int planetX, int planetY, List<String> planetObstacles, int x, int y, char direction)
			throws MarsRoverException {
		this.planet = new Cell[planetX][planetY];
		for (int i = 0; i < planetX; i++) {
			for (int j = 0; j < planetY; j++) {
				this.planet[i][j] = new Cell(i, j, false);
			}
		}
		for (int k = 0; k < planetObstacles.size(); k++) {
			String rawObstacle = planetObstacles.get(k);
			String obstacleCoord = rawObstacle.substring(1, rawObstacle.length() - 1);
			String[] coordArray = obstacleCoord.split(",");
			this.planet[Integer.parseInt(coordArray[0])][Integer.parseInt(coordArray[1])].isObstacle = true;
		}

		this.x = x;
		this.y = y;
		this.direction = direction;
	}

	/**
	 * It returns whether, or not, the planet (where the rover moves) contains an
	 * obstacle in a cell.
	 * 
	 * @param x The x coordinate of the cell
	 * @param y The y coordinate of the cell
	 * @return <true> if the cell contains an obstacle, <false> otherwise.
	 * @throws MarsRoverException
	 */
	public boolean planetContainsObstacleAt(int x, int y) throws MarsRoverException {
		return this.planet[x][y].isObstacle;
	}

	/**
	 * It lets the rover move on the planet according to a command string. The
	 * return string contains the new position of the rover, its direction, and the
	 * obstacles it has encountered while moving on the planet (if any).
	 * 
	 * @param commandString A string that can contain a single command -- i.e. "f"
	 *                      (forward), "b" (backward), "l" (left), or "r" (right) --
	 *                      or a combination of single commands.
	 * @return The return string that contains the position and direction of the
	 *         rover, and the obstacles the rover has encountered while moving on
	 *         the planet (if any). The return string (without white spaces) has the
	 *         following format: "(x,y,dir)(o1_x,o1_y)(o2_x,o2_y)...(on_x,on_y)". x
	 *         and y define the new position of the rover while dir represents its
	 *         direction (i.e., N, S, W, or E). Finally, oi_x and oi_y are the
	 *         coordinates of the i-th encountered obstacle.
	 * @throws MarsRoverException
	 */
	public String executeCommand(String commandString) throws MarsRoverException {
		for (int i = 0; i < commandString.length(); i++) {
			switch (this.direction) {
			case 'N':
				moveFacingNord(commandString.charAt(i));
				break;
			case 'S':
				moveFacingSud(commandString.charAt(i));
				break;
			case 'W':
				moveFacingWest(commandString.charAt(i));
				break;
			case 'E':
				moveFacingEast(commandString.charAt(i));
				break;
			}
		}
		return "(" + this.x + "," + this.y + "," + this.direction + ")";
	}

	private void moveFacingNord(char command) {
		if (command == 'f') {
			if (this.y == 9) {
				this.y = 0;
			} else
				this.y++;
		} else if (command == 'b') {
			if (this.y == 0) {
				this.y = 9;
			} else
				this.y--;
		} else if (command == 'r') {
			this.direction = 'E';
		} else if (command == 'l') {
			this.direction = 'W';
		}
	}

	private void moveFacingSud(char command) {
		if (command == 'f') {
			if (this.y == 0) {
				this.y = 9;
			} else
				this.y--;
		} else if (command == 'b') {
			if (this.y == 9) {
				this.y = 0;
			} else
				this.y++;
		} else if (command == 'r') {
			this.direction = 'W';
		} else if (command == 'l') {
			this.direction = 'E';
		}
	}
	
	private void moveFacingWest(char command) {
		if (command == 'f') {
			if (this.x == 0) {
				this.x = 9;
			} else
				this.x--;
		} else if (command == 'b') {
			if (this.x == 9) {
				this.x = 0;
			} else
				this.x++;
		} else if (command == 'r') {
			this.direction = 'N';
		} else if (command == 'l') {
			this.direction = 'S';
		}
	}
	
	private void moveFacingEast(char command) {
		if (command == 'f') {
			if (this.x == 9) {
				this.x = 0;
			} else
				this.x++;
		} else if (command == 'b') {
			if (this.x == 0) {
				this.x = 9;
			} else
				this.x--;
		} else if (command == 'r') {
			this.direction = 'S';
		} else if (command == 'l') {
			this.direction = 'N';
		}
	}

}
